package muic.design.pattern.api.constant;

/**
 * Created by Trung on 11/27/2017.
 */
public class Constant {

    public static final String SELF_IP = "localhost";

    public static final String SELF_PORT = "8080";

    public static final String DATABASE = "database";

    public static final String DATABASE_INFORMATION = "database_info";

    public static final String CLIENT = "client";

    public static final String USER = "user";
}
