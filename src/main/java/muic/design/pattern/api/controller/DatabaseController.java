package muic.design.pattern.api.controller;

import com.google.gson.JsonObject;
import muic.design.pattern.api.constant.Constant;
import muic.design.pattern.api.dto.DatabaseDto;
import muic.design.pattern.api.dto.DatabaseInformationWrapperDto;
import muic.design.pattern.api.model.store.DatabaseInformation;
import muic.design.pattern.api.service.ClientService;
import muic.design.pattern.api.service.DatabaseInformationService;
import muic.design.pattern.api.service.DatabaseInformationWrapperService;
import muic.design.pattern.api.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(Constant.DATABASE)
public class DatabaseController {

    @Autowired
    DatabaseInformationWrapperService databaseInformationWrapperService;

    @Autowired
    DatabaseService databaseService;

    @Autowired
    ClientService clientService;

    /**
     * Get all databases
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<DatabaseInformationWrapperDto> getAllDatabase() throws Exception {
        List<String> databaseIdList = databaseService.findAllDatabaseId();
        List<DatabaseInformationWrapperDto> databaseInformationWrapperDtoList =
                new ArrayList<DatabaseInformationWrapperDto>();

        for (String id: databaseIdList) {
            DatabaseInformationWrapperDto databaseInformationWrapperDto =
                    databaseInformationWrapperService.parseByDatabaseId(id);
            databaseInformationWrapperDtoList.add(databaseInformationWrapperDto);
        }

        return databaseInformationWrapperDtoList;
    }

    /**
     * Get database dto
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public DatabaseDto getDatabase(@RequestParam(value = "id") String id) throws Exception{
        DatabaseDto databaseDto = databaseService.findDatabaseDtoById(id);

        return databaseDto;
    }

    /**
     * Add database
     * @param databaseDto
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addDatabase(@RequestBody DatabaseDto databaseDto) throws Exception{
        JsonObject jsonObject = new JsonObject();

        String ip = databaseDto.getIp();
        Long port = databaseDto.getPort();

        // Connect to database
        boolean isAliveClient = clientService.connect(ip, port, ip);
        jsonObject.addProperty("message:", isAliveClient);

        if (isAliveClient)
            databaseService.add(databaseDto);
        else
            jsonObject.addProperty("error", "client is not alive");

        return jsonObject.toString();
    }

    /**
     * Delete database
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String removeDatabase(@RequestParam(value = "id") String id) throws Exception {
        JsonObject jsonObject = new JsonObject();
        databaseService.delete(id);

        jsonObject.addProperty("status:", 200);
        return jsonObject.toString();
    }
}
