package muic.design.pattern.api.controller;

import muic.design.pattern.api.constant.Constant;
import muic.design.pattern.api.dto.ClientDto;
import muic.design.pattern.api.dto.DatabaseDto;
import muic.design.pattern.api.dto.DatabaseInformationDto;
import muic.design.pattern.api.service.DatabaseInformationService;
import muic.design.pattern.api.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Trung on 11/27/2017.
 */
@RestController
@RequestMapping(Constant.CLIENT)
public class ClientController {

    @Autowired
    DatabaseService databaseService;

    @Autowired
    DatabaseInformationService databaseInformationService;

    /**
     * Edit database information with client's information
     * @param clientDto
     * @throws Exception
     */
    @RequestMapping(value = "/response", method = RequestMethod.POST)
    public void editInformation(@RequestBody ClientDto clientDto) throws Exception{
        DatabaseDto databaseDto = databaseService.findDatabaseDtoByIp(clientDto.getKey());
        String databaseId = databaseDto.getId();

        DatabaseInformationDto databaseInformationDto = databaseInformationService.
                findDatabaseInformationDtoByDatabaseId(databaseId);

        databaseInformationService.edit(databaseInformationDto, clientDto);
    }
}
