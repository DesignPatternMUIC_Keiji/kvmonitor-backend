package muic.design.pattern.api.controller;

import com.google.gson.JsonObject;
import muic.design.pattern.api.constant.Constant;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by Trung on 11/27/2017.
 */
@RestController
@RequestMapping(Constant.USER)
public class UserController {

    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public final String isAuthenticated(final Principal principal) {
        JsonObject jsonObject = new JsonObject();

        if (principal == null) {
            jsonObject.addProperty("authenticated", false);
        } else {
            jsonObject.addProperty("authenticated", true);
        }

        return jsonObject.toString();
    }
}
