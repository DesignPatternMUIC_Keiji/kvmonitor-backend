package muic.design.pattern.api.controller;

import muic.design.pattern.api.constant.Constant;
import muic.design.pattern.api.dto.DatabaseDto;
import muic.design.pattern.api.dto.DatabaseInformationDto;
import muic.design.pattern.api.model.store.DatabaseInformation;
import muic.design.pattern.api.service.ClientService;
import muic.design.pattern.api.service.DatabaseInformationService;
import muic.design.pattern.api.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constant.DATABASE_INFORMATION)
public class DatabaseInformationController {

    @Autowired
    DatabaseInformationService databaseInformationService;

    /**
     * Get database information by database id
     * @param databaseId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public DatabaseInformationDto getDatabase(@RequestParam(value = "databaseId") String databaseId) throws Exception{
        DatabaseInformationDto databaseInformationDto = databaseInformationService.
                findDatabaseInformationDtoByDatabaseId(databaseId);

        return databaseInformationDto;
    }
}
