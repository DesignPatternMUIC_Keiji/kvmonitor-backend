package muic.design.pattern.api.service;

import muic.design.pattern.api.dto.ClientDto;
import muic.design.pattern.api.dto.DatabaseInformationDto;

/**
 * Created by Trung on 11/27/2017.
 */
public interface DatabaseInformationService {

    /**
     * Find databaseInformation by databaseId
     * @param databaseId
     * @return
     */
    DatabaseInformationDto findDatabaseInformationDtoByDatabaseId(String databaseId);

    /**
     * Add databaseInformation to datbase
     * @param databaseInformationDto
     */
    void add(DatabaseInformationDto databaseInformationDto);

    /**
     * Edit databaseInformation based on clientDto
     * @param databaseInformationDto
     * @param clientDto
     */
    void edit(DatabaseInformationDto databaseInformationDto, ClientDto clientDto);
}
