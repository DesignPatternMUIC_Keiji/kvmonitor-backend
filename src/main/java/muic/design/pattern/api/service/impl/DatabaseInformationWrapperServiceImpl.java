package muic.design.pattern.api.service.impl;

import muic.design.pattern.api.dto.DatabaseDto;
import muic.design.pattern.api.dto.DatabaseInformationDto;
import muic.design.pattern.api.dto.DatabaseInformationWrapperDto;
import muic.design.pattern.api.service.CalculationService;
import muic.design.pattern.api.service.DatabaseInformationService;
import muic.design.pattern.api.service.DatabaseInformationWrapperService;
import muic.design.pattern.api.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Trung on 11/28/2017.
 */
@Service
public class DatabaseInformationWrapperServiceImpl implements DatabaseInformationWrapperService {

    @Autowired
    CalculationService calculationService;

    @Autowired
    DatabaseService databaseService;

    @Autowired
    DatabaseInformationService databaseInformationService;

    /**
     * Parse corresponding data into DatabaseInformationWrapperDto
     * @param id
     * @return
     */
    @Override
    public DatabaseInformationWrapperDto parseByDatabaseId(String id) throws Exception{
        DatabaseInformationWrapperDto databaseInformationWrapperDto = new DatabaseInformationWrapperDto();
        DatabaseDto databaseDto = databaseService.findDatabaseDtoById(id);
        DatabaseInformationDto databaseInformationDto = databaseInformationService.
                findDatabaseInformationDtoByDatabaseId(id);

        String status = calculationService.calculateStatusByCpuUsage(databaseInformationDto.getCpuUsage());

        databaseInformationWrapperDto.setId(databaseDto.getId());
        databaseInformationWrapperDto.setIp(databaseDto.getIp());
        databaseInformationWrapperDto.setPort(databaseDto.getPort());
        databaseInformationWrapperDto.setName(databaseDto.getName());
        databaseInformationWrapperDto.setType(databaseDto.getType());
        databaseInformationWrapperDto.setAverageCpuUsage(databaseInformationDto.getAverageCpuUsage());
        databaseInformationWrapperDto.setAverageRequestPerSecond(databaseInformationDto.getAverageRequestPerSecond());
        databaseInformationWrapperDto.setStatus(status);

        return databaseInformationWrapperDto;
    }
}
