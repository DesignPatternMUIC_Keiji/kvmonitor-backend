package muic.design.pattern.api.service.impl;

import muic.design.pattern.api.model.CpuUsageByDate;
import muic.design.pattern.api.model.RequestPerSecondByDate;
import muic.design.pattern.api.service.CalculationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Trung on 11/28/2017.
 */
@Service
public class CalculationServiceImpl implements CalculationService {

    /**
     * Calculate Status
     * @param cpuUsageByDateList
     * @return
     */
    @Override
    public String calculateStatusByCpuUsage(List<CpuUsageByDate> cpuUsageByDateList) {
        Double averageCpuUsage = this.calculateAverageCpuUsage(cpuUsageByDateList);

        if (averageCpuUsage <= 30) return "Healthy";
        else if (averageCpuUsage <= 60) return "Average";
        else return "Bad";
    }

    /**
     * Calculate average cpu usage
     * @param cpuUsageByDateList
     * @return
     */
    @Override
    public Double calculateAverageCpuUsage(List<CpuUsageByDate> cpuUsageByDateList) {
        Double sumRecentCpuUsage = 0D;
        List<CpuUsageByDate> recentCpuUsageByDate;

        if (cpuUsageByDateList.size() >= 5) {
            recentCpuUsageByDate = cpuUsageByDateList
                    .subList(cpuUsageByDateList.size() - 5, cpuUsageByDateList.size());
        } else {
            recentCpuUsageByDate = cpuUsageByDateList;
        }

        for (CpuUsageByDate cpuUsageByDate : recentCpuUsageByDate) {
            sumRecentCpuUsage += cpuUsageByDate.getCpuUsage();
        }

        return sumRecentCpuUsage / recentCpuUsageByDate.size();
    }

    /**
     * Calculate average request per second
     * @param requestPerSecondByDateList
     * @return
     */
    @Override
    public Double calculateAverageRequestPerSecond(List<RequestPerSecondByDate> requestPerSecondByDateList) {
        Double sumRecentRequestPerSecond = 0D;
        List<RequestPerSecondByDate> recentRequestPerSecond;

        if (requestPerSecondByDateList.size() >= 5) {
            recentRequestPerSecond = requestPerSecondByDateList
                    .subList(requestPerSecondByDateList.size() - 5, requestPerSecondByDateList.size());
        } else {
            recentRequestPerSecond = requestPerSecondByDateList;
        }

        for (RequestPerSecondByDate requestPerSecondByDate : recentRequestPerSecond) {
            sumRecentRequestPerSecond += requestPerSecondByDate.getRequestPerSecond();
        }

        return sumRecentRequestPerSecond / recentRequestPerSecond.size();
    }
}
