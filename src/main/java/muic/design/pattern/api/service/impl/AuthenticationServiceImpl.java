package muic.design.pattern.api.service.impl;

import muic.design.pattern.api.service.AuthenticationService;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Trung on 11/18/2017.
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    /**
     * Authenticate input username/password
     * File must contain only 2 lines
     * @param username
     * @param password
     * @return
     */
    public boolean authenticate(String username, String password){
        String filename = "account.txt";

        String line;
        boolean usernameFlag = false;
        boolean passwordFlag = false;

        try {
            FileReader fileReader = new FileReader(filename);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null && (!usernameFlag || !passwordFlag)) {
                // Check username
                if (line.startsWith("username:")) {
                    usernameFlag = true;
                    String realUsername = line.split(":")[1];
                    if (!realUsername.equals(username)) return false;
                }
                // Check password
                if (line.startsWith("password:")) {
                    passwordFlag = true;
                    String realPassword = line.split(":")[1];
                    if (!realPassword.equals(password)) return false;
                }
            }
            return usernameFlag && passwordFlag;
        } catch (IOException e) { e.printStackTrace(); }

        // File doesn't exist
        return false;
    }
}
