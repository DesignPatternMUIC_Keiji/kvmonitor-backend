package muic.design.pattern.api.service;

import muic.design.pattern.api.model.CpuUsageByDate;
import muic.design.pattern.api.model.RequestPerSecondByDate;

import java.util.List;

/**
 * Created by Trung on 11/28/2017.
 */
public interface CalculationService {

    /**
     * Calculate status given cpuUsageList
     * @param cpuUsageByDateList
     * @return
     */
    String calculateStatusByCpuUsage(List<CpuUsageByDate> cpuUsageByDateList);

    /**
     * Calculate average cpu usage
     * @param cpuUsageByDateList
     * @return
     */
    Double calculateAverageCpuUsage(List<CpuUsageByDate> cpuUsageByDateList);

    /**
     * Calculate average request per second
     * @param requestPerSecondByDateList
     * @return
     */
    Double calculateAverageRequestPerSecond(List<RequestPerSecondByDate> requestPerSecondByDateList);
}
