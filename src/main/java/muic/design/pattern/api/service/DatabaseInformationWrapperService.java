package muic.design.pattern.api.service;

import muic.design.pattern.api.dto.DatabaseInformationWrapperDto;

/**
 * Created by Trung on 11/28/2017.
 */
public interface DatabaseInformationWrapperService {

    /**
     * Find corresponding databaseDto and databaseInformationDto
     * Parse them into DatabaseInformationWrapperDtos
     * @param id
     * @return
     */
    DatabaseInformationWrapperDto parseByDatabaseId(String id) throws Exception;
}
