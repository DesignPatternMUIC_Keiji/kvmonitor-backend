package muic.design.pattern.api.service.impl;

import com.google.common.collect.Lists;
import muic.design.pattern.api.dto.DatabaseDto;
import muic.design.pattern.api.dto.DatabaseInformationDto;
import muic.design.pattern.api.model.store.Database;
import muic.design.pattern.api.repository.DatabaseRepository;
import muic.design.pattern.api.service.DatabaseInformationService;
import muic.design.pattern.api.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trung on 11/27/2017.
 */
@Service
public class DatabaseServiceImpl implements DatabaseService {

    @Autowired
    DatabaseInformationService databaseInformationService;

    @Autowired
    DatabaseRepository databaseRepository;

    /**
     * Find database dto by id
     * @param id
     * @return
     */
    @SuppressWarnings("Duplicates")
    @Override
    public DatabaseDto findDatabaseDtoById(String id) throws Exception {
        DatabaseDto databaseDto = new DatabaseDto();
        Database database = databaseRepository.findOne(id);

        if (database != null) {
            databaseDto.setId(database.getId());
            databaseDto.setIp(database.getIp());
            databaseDto.setName(database.getName());
            databaseDto.setPort(database.getPort());
            databaseDto.setType(database.getType());
        } else {
            throw new Exception("Database with id: " + id + " not found!"   );
        }

        return databaseDto;
    }

    /**
     * Return database by ip
     * @param ip
     * @return
     */
    @SuppressWarnings("Duplicates")
    @Override
    public DatabaseDto findDatabaseDtoByIp(String ip) throws Exception {
        DatabaseDto databaseDto = new DatabaseDto();
        Database database = databaseRepository.findByIp(ip);

        if (database != null) {
            databaseDto.setId(database.getId());
            databaseDto.setIp(database.getIp());
            databaseDto.setName(database.getName());
            databaseDto.setPort(database.getPort());
            databaseDto.setType(database.getType());
        } else {
            throw new Exception();
        }

        return databaseDto;
    }

    /**
     * Get all databases
     * @return
     */
    @Override
    public List<DatabaseDto> findAllDatabaseDto() {
        List<Database> databaseList = Lists.newArrayList(databaseRepository.findAll());
        List<DatabaseDto> databaseDtoList = new ArrayList<DatabaseDto>();

        for (Database database: databaseList) {
            DatabaseDto databaseDto = new DatabaseDto();

            databaseDto.setId(database.getId());
            databaseDto.setType(database.getType());
            databaseDto.setPort(database.getPort());
            databaseDto.setName(database.getName());
            databaseDto.setIp(database.getIp());

            databaseDtoList.add(databaseDto);
        }

        return databaseDtoList;
    }

    /**
     * Find all database Id
     * @return
     */
    @Override
    public List<String> findAllDatabaseId() {
        List<String> idList = new ArrayList<String>();
        List<Database> databaseList = Lists.newArrayList(databaseRepository.findAll());

        for (Database database: databaseList) idList.add(database.getId());

        return idList;
    }

    /**
     * Add new database
     * @param databaseDto
     */
    @Override
    public void add(DatabaseDto databaseDto) {
        Database database = new Database();

        database.setIp(databaseDto.getIp());
        database.setName(databaseDto.getName());
        database.setPort(databaseDto.getPort());
        database.setType(databaseDto.getType());

        Database savedDatabase = databaseRepository.save(database);
        String id = savedDatabase.getId();

        // Save corresponding database information
        DatabaseInformationDto databaseInformationDto = new DatabaseInformationDto();
        databaseInformationDto.setDatabaseId(id);
        databaseInformationService.add(databaseInformationDto);
    }

    /**
     * Delete Database
     * @param id
     */
    @Override
    public void delete(String id) {
        databaseRepository.delete(id);
    }


}
