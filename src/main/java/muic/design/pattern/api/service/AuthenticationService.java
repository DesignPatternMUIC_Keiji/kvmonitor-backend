package muic.design.pattern.api.service;

/**
 * Created by Trung on 11/18/2017.
 */
public interface AuthenticationService {

    /**
     * Authenticate input username/password
     * @param username
     * @param password
     * @return
     */
    boolean authenticate(String username, String password);
}
