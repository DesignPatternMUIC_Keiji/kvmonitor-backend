package muic.design.pattern.api.service;

/**
 * Created by Trung on 11/27/2017.
 */
public interface ClientService {

    /**
     * Create connection to client
     * @param key
     * @param ip
     * @param port
     */
    boolean connect(String ip, Long port, String key) throws Exception;
}
