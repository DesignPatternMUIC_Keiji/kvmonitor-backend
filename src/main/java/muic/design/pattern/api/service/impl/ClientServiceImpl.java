package muic.design.pattern.api.service.impl;

import muic.design.pattern.api.constant.Constant;
import muic.design.pattern.api.service.ClientService;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Trung on 11/27/2017.
 */
@Service
public class ClientServiceImpl implements ClientService{

    /**
     * Connect to client
     * @param key
     * @param ip
     * @param port
     */
    @Override
    public boolean connect(String ip, Long port, String key) throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");

        headers.setAll(map);

        Map req_payload = new HashMap();
        req_payload.put("ip", Constant.SELF_IP);
        req_payload.put("port", Constant.SELF_PORT);
        req_payload.put("key", key);

        HttpEntity<?> request = new HttpEntity(req_payload, headers);
        String clientURI = "http://".concat(ip).concat(":").concat(port.toString()).concat("/connect");

        try {
            ResponseEntity<?> response = new RestTemplate().postForEntity(clientURI, request, String.class);

            return response.getStatusCodeValue() == 200;
        } catch (Exception e) {
            return false;
        }
    }
}
