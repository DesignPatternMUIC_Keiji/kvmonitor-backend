package muic.design.pattern.api.service;

import muic.design.pattern.api.dto.DatabaseDto;

import java.util.List;

/**
 * Created by Trung on 11/27/2017.
 */
public interface DatabaseService {

    /**
     * Find dto by database id
     * @param id
     * @return
     */
    DatabaseDto findDatabaseDtoById(String id) throws Exception;

    /**
     * Return database by ip
     * @param ip
     * @return
     */
    DatabaseDto findDatabaseDtoByIp(String ip) throws Exception;

    /**
     * Find all database dto
     * @return
     */
    List<DatabaseDto> findAllDatabaseDto();

    /**
     * Find all database id
     * @return
     */
    List<String> findAllDatabaseId();

    /**
     * Add new database
     * @param databaseDto
     */
    void add(DatabaseDto databaseDto);

    /**
     * Delete databases
     * @param id
     */
    void delete(String id);
}
