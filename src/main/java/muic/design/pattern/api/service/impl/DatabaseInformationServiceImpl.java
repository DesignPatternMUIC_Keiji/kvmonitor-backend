package muic.design.pattern.api.service.impl;

import muic.design.pattern.api.dto.ClientDto;
import muic.design.pattern.api.dto.DatabaseInformationDto;
import muic.design.pattern.api.model.CpuUsageByDate;
import muic.design.pattern.api.model.RequestPerSecondByDate;
import muic.design.pattern.api.model.store.DatabaseInformation;
import muic.design.pattern.api.repository.DatabaseInformationRepository;
import muic.design.pattern.api.service.CalculationService;
import muic.design.pattern.api.service.DatabaseInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Trung on 11/27/2017.
 */
@Service
public class DatabaseInformationServiceImpl implements DatabaseInformationService {

    @Autowired
    CalculationService calculationService;

    @Autowired
    DatabaseInformationRepository databaseInformationRepository;

    /**
     * Get databaseInformationDto by databaseId
     * @param databaseId
     * @return
     */
    @Override
    public DatabaseInformationDto findDatabaseInformationDtoByDatabaseId(String databaseId) {
        DatabaseInformationDto databaseInformationDto = new DatabaseInformationDto();
        DatabaseInformation databaseInformation = databaseInformationRepository.findByDatabaseId(databaseId);

        String status = calculationService
                .calculateStatusByCpuUsage(databaseInformation.getCpuUsage());
        Double averageCpuUsage = calculationService
                .calculateAverageCpuUsage(databaseInformation.getCpuUsage());
        Double averageRequestPerSecond = calculationService
                .calculateAverageRequestPerSecond(databaseInformation.getRequestPerSecond());

        databaseInformationDto.setDatabaseId(databaseInformation.getDatabaseId());
        databaseInformationDto.setCpuUsage(databaseInformation.getCpuUsage());
        databaseInformationDto.setRequestPerSecond(databaseInformation.getRequestPerSecond());
        databaseInformationDto.setAverageCpuUsage(averageCpuUsage);
        databaseInformationDto.setAverageRequestPerSecond(averageRequestPerSecond);
        databaseInformationDto.setStatus(status);

        return databaseInformationDto;
    }

    /**
     * Add database information
     * @param databaseInformationDto
     */
    @Override
    public void add(DatabaseInformationDto databaseInformationDto) {
        String databaseId = databaseInformationDto.getDatabaseId();
        DatabaseInformation databaseInformation = new DatabaseInformation();

        // Set initial cpuUsage
        List<CpuUsageByDate> cpuUsageByDateList = new ArrayList<CpuUsageByDate>();
        CpuUsageByDate cpuUsageByDate = new CpuUsageByDate();

        cpuUsageByDate.setCpuUsage(0D);
        cpuUsageByDate.setDate(new Date());

        cpuUsageByDateList.add(cpuUsageByDate);

        // Set initial RequestPerSecond
        List<RequestPerSecondByDate> requestPerSecondByDateList = new ArrayList<RequestPerSecondByDate>();
        RequestPerSecondByDate requestPerSecondByDate = new RequestPerSecondByDate();

        requestPerSecondByDate.setRequestPerSecond(0D);
        requestPerSecondByDate.setDate(new Date());

        requestPerSecondByDateList.add(requestPerSecondByDate);

        // Save databaseInformation
        databaseInformation.setDatabaseId(databaseId);
        databaseInformation.setCpuUsage(cpuUsageByDateList);
        databaseInformation.setRequestPerSecond(requestPerSecondByDateList);
        databaseInformation.setStatus("Healthy");

        databaseInformationRepository.save(databaseInformation);
    }

    /**
     * Edit databaseInformation
     * @param databaseInformationDto
     * @param clientDto
     */
    @Override
    public void edit(DatabaseInformationDto databaseInformationDto, ClientDto clientDto) {
        String databaseId = databaseInformationDto.getDatabaseId();
        DatabaseInformation oldDatabaseInformation = databaseInformationRepository.findByDatabaseId(databaseId);
        DatabaseInformation newDatabaseInformation = new DatabaseInformation();

        List<CpuUsageByDate> cpuUsageByDateList = oldDatabaseInformation.getCpuUsage();
        List<RequestPerSecondByDate> requestPerSecondByDateList = oldDatabaseInformation.getRequestPerSecond();

        // Build CpuUsageByDate
        CpuUsageByDate cpuUsageByDate = new CpuUsageByDate();
        cpuUsageByDate.setCpuUsage(clientDto.getCpuUsage());
        cpuUsageByDate.setDate(new Date());

        // Build RequestPerSecondByDate
        RequestPerSecondByDate requestPerSecondByDate = new RequestPerSecondByDate();
        requestPerSecondByDate.setRequestPerSecond(clientDto.getRequestPerSec());
        requestPerSecondByDate.setDate(new Date());

        // Add to new list
        cpuUsageByDateList.add(cpuUsageByDate);
        requestPerSecondByDateList.add(requestPerSecondByDate);

        newDatabaseInformation.setId(oldDatabaseInformation.getId());
        newDatabaseInformation.setDatabaseId(databaseId);
        newDatabaseInformation.setCpuUsage(cpuUsageByDateList);
        newDatabaseInformation.setRequestPerSecond(requestPerSecondByDateList);
        newDatabaseInformation.setStatus(databaseInformationDto.getStatus());

        databaseInformationRepository.save(newDatabaseInformation);
    }
}
