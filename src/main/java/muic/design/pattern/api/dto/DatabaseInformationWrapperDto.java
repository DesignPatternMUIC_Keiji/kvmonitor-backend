package muic.design.pattern.api.dto;

import muic.design.pattern.api.model.CpuUsageByDate;
import muic.design.pattern.api.model.RequestPerSecondByDate;

import java.util.List;

/**
 * Created by Trung on 11/28/2017.
 */
public class DatabaseInformationWrapperDto {

    private String id;

    private String name;

    private String ip;

    private Long port;

    private String type;

    private String status;

    private Double averageCpuUsage;

    private Double averageRequestPerSecond;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getAverageCpuUsage() {
        return averageCpuUsage;
    }

    public void setAverageCpuUsage(Double averageCpuUsage) {
        this.averageCpuUsage = averageCpuUsage;
    }

    public Double getAverageRequestPerSecond() {
        return averageRequestPerSecond;
    }

    public void setAverageRequestPerSecond(Double averageRequestPerSecond) {
        this.averageRequestPerSecond = averageRequestPerSecond;
    }
}
