package muic.design.pattern.api.dto;

/**
 * Created by Trung on 11/19/2017.
 */
public class ClientDto {

    String key;

    Double cpuUsage;

    Double requestPerSec;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Double getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(Double cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public Double getRequestPerSec() {
        return requestPerSec;
    }

    public void setRequestPerSec(Double requestPerSec) {
        this.requestPerSec = requestPerSec;
    }

}
