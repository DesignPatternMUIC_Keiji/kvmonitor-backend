package muic.design.pattern.api.dto;

import muic.design.pattern.api.model.CpuUsageByDate;
import muic.design.pattern.api.model.RequestPerSecondByDate;

import java.util.List;

/**
 * Created by Trung on 11/19/2017.
 */
public class DatabaseInformationDto {

    private String databaseId;

    private String status;

    private Double averageCpuUsage;

    private Double averageRequestPerSecond;

    private List<CpuUsageByDate> cpuUsage;

    private List<RequestPerSecondByDate> requestPerSecond;

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getAverageCpuUsage() {
        return averageCpuUsage;
    }

    public void setAverageCpuUsage(Double averageCpuUsage) {
        this.averageCpuUsage = averageCpuUsage;
    }

    public Double getAverageRequestPerSecond() {
        return averageRequestPerSecond;
    }

    public void setAverageRequestPerSecond(Double averageRequestPerSecond) {
        this.averageRequestPerSecond = averageRequestPerSecond;
    }

    public List<CpuUsageByDate> getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(List<CpuUsageByDate> cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public List<RequestPerSecondByDate> getRequestPerSecond() {
        return requestPerSecond;
    }

    public void setRequestPerSecond(List<RequestPerSecondByDate> requestPerSecond) {
        this.requestPerSecond = requestPerSecond;
    }
}
