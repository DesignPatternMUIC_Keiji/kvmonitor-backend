package muic.design.pattern.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by Trung on 11/27/2017.
 */
public class RequestPerSecondByDate {

    private Double requestPerSecond;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date date;

    public Double getRequestPerSecond() {
        return requestPerSecond;
    }

    public void setRequestPerSecond(Double requestPerSecond) {
        this.requestPerSecond = requestPerSecond;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
