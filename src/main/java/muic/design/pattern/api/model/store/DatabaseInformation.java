package muic.design.pattern.api.model.store;

import muic.design.pattern.api.constant.Constant;
import muic.design.pattern.api.model.CpuUsageByDate;
import muic.design.pattern.api.model.RequestPerSecondByDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.List;

/**
 * Created by Trung on 11/19/2017.
 */
@RedisHash(Constant.DATABASE_INFORMATION)
public class DatabaseInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Indexed
    private String databaseId;

    private String type;

    private String status;

    private List<CpuUsageByDate> cpuUsage;

    private List<RequestPerSecondByDate> requestPerSecond;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CpuUsageByDate> getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(List<CpuUsageByDate> cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public List<RequestPerSecondByDate> getRequestPerSecond() {
        return requestPerSecond;
    }

    public void setRequestPerSecond(List<RequestPerSecondByDate> requestPerSecond) {
        this.requestPerSecond = requestPerSecond;
    }


}
