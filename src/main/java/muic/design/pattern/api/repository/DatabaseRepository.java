package muic.design.pattern.api.repository;

import muic.design.pattern.api.model.store.Database;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Trung on 11/27/2017.
 */
public interface DatabaseRepository extends CrudRepository<Database, String>{
    
    /**
     * @param ip
     * @return
     */
    Database findByIp(String ip);
}
