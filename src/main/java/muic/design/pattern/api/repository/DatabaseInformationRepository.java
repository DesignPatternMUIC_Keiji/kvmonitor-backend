package muic.design.pattern.api.repository;

import muic.design.pattern.api.model.store.DatabaseInformation;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Trung on 11/27/2017.
 */
public interface DatabaseInformationRepository extends CrudRepository<DatabaseInformation, String> {

    /**
     * Return database information by database id
     * @param databaseid
     * @return
     */
    DatabaseInformation findByDatabaseId(String databaseid);
}
