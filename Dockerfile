FROM maven:3.3.9-onbuild-alpine
VOLUME /tmp

ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /usr/src/app/target/webwar-0.0.1-SNAPSHOT.jar" ]
